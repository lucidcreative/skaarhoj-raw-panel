"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var net_1 = require("net");
var carrier = __importStar(require("carrier"));
var PanelServer = /** @class */ (function () {
    function PanelServer(_port) {
        if (_port === void 0) { _port = 9923; }
        this._port = _port;
        this.processPanelMessage = function (socket) { return function (buffer) {
            var data = buffer.toString();
            console.log(data);
            switch (data) {
                case 'list':
                    socket.write(Buffer.from("\nActivePanel=1\n"));
                    break;
                case 'ping':
                    socket.write(Buffer.from("ack\n"));
                    break;
            }
        }; };
        this._connections = [];
        this._server = net_1.createServer({ allowHalfOpen: true }, this.handleConnection);
        this._server.listen(this._port);
    }
    PanelServer.prototype.handleConnection = function (socket) {
        var _this = this;
        console.log("Connection from " + socket.remoteAddress);
        // this._connections.concat([socket])
        carrier.carry(socket, this.processPanelMessage(socket));
        socket.on('close', function () { return _this._connections = _this._connections.filter(function (x) { return x !== socket; }); });
    };
    return PanelServer;
}());
exports.PanelServer = PanelServer;
//# sourceMappingURL=PanelServer.js.map