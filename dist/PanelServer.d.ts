/// <reference types="node" />
import { Socket } from 'net';
export declare class PanelServer {
    private _port;
    private _server;
    private _connections;
    constructor(_port?: number);
    handleConnection(socket: Socket): void;
    processPanelMessage: (socket: Socket) => (buffer: Buffer) => void;
}
