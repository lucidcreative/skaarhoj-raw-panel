import { HardwareComponentType, Shape } from './skaarhoj';
import { inspect } from 'util';

export function lookupType(id: number, data: Shape): HardwareComponentType {
	switch (id) {
		case 133: // 4 way soft button
			return {
				id,
				shape: data,
				description: "4 Way Soft Button + LCD",
				hasText: true,
				hasColor: true
			}
		case 141: // 3 LED horizontal indicator
			return {
				id,
				shape: data,
				description: "3 LED horizontal indicator",
				hasText: false,
				hasColor: true,
			}
		case 250: // section or controller
			return {
				id,
				shape: data,
				description: "Group",
				hasText: false,
				hasColor: false,
			}
		default:
			throw new Error(`HWC type ${id} is not implemented! Data payload: ${inspect(data, false, 4, true)}`)
	}
}