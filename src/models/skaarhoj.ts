export interface HardwareComponent {
	id: number
	readonly position: {
		x: number
		y: number
	}
	readonly type: HardwareComponentType
	readonly name: string
	text?: TextOptions
	color?: string
	brightness?: Brightness
}

export interface HardwareComponentType {
	id: number
	shape: Shape
	description: string
	hasText: boolean
	hasColor: boolean
}

export interface Shape {
	w: number
	h: number
	sub: SubShape[]
}

interface SubShape {
	_: string
}

export interface Rectangle extends SubShape {
	_: 'r'
	_x: number
	_y: number
	_w: number
	_h: number
	rx: number
	ry: number
	style: string
}

export interface Circle {
	_: 'c'
	_x: number
	_y: number
	r: number
}

export const enum Brightness {
	Off = 0,
	Red = 34,
	Green = 35,
	Dimmed = 5,
	Full = 36,
}

export interface TextOptions {
	title: string
	valueOptions?: {
		value1: number
		value2?: number
		format: ValueFormat
		useSmallText: boolean
		fine: boolean
		label1?: string
		label2?: string
		valuePairing?: ValueParing

	}
	isLabel: boolean
}

export const enum ValueFormat {
	Integer = 0,
	Scientific = 1,
	Percent = 2,
	dB = 3,
	Frames = 4,
	OneOverValue = 5,
	Kelvin = 6,
	Hidden = 7
}

export const enum ValueParing {
	A = 1,
	B = 2,
	C = 3,
	D = 4,
}

export interface HWCEvent {
	id: number
	mask?: number
	action: HWCAction
	value?: number
}

export const enum HWCAction {
	Press = 'Press',
	Down = 'Down',
	Up = 'Up',
	Absolute = 'Abs',
	Speed = 'Speed',
	Relative = 'Enc'
}

export interface PropertyChangeEvent {
	key: string
}

// log events
export const enum LogLevel {
	Debug = 10,
	Info = 20,
	Warn = 30,
	Error = 40,
}

export interface LogEvent {
	level: LogLevel
	message: string
	code?: string
	data: {}
	timestamp: number
}

export interface DebugLogEvent extends LogEvent {
	level: LogLevel.Debug
}

export interface InfoLogEvent extends LogEvent {
	level: LogLevel.Info
}

export interface WarnLogEvent extends LogEvent {
	level: LogLevel.Warn
}

export interface ErrorLogEvent extends LogEvent {
	level: LogLevel.Error
}
