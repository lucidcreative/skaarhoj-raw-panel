import { SkaarhojServer } from '../skaarhojServer'
import { createConnection, Socket } from 'net';
import { SkaarhojPanel } from '../skaarhojPanel';
import { Duplex } from 'stream';


const mockServerListen = jest.fn()
const mockServerClose = jest.fn()
let mockConnect: (socket: Socket) => void
jest.mock('net', () => {
	return {
		createServer: (cb: () => {}) => {
			mockConnect = cb
			return {
				listen: mockServerListen,
				close: mockServerClose
			}
		}
	}
})


describe('SkaarhojServer class', () => {
	it("creates an instance", () => {
		const x = new SkaarhojServer()
		expect(x).toBeInstanceOf(SkaarhojServer)
	})

	describe('instance tests', () => {
		let server: SkaarhojServer,
			mockConnectHandler: jest.Mock

		beforeAll(() => {
			mockConnectHandler = jest.fn()
			server = new SkaarhojServer(mockConnectHandler)
		})
		beforeEach(() => {
			jest.clearAllMocks()
		})

		it('should return a skaarhoj panel on connect', async (done) => {
			mockConnectHandler.mockImplementationOnce(() => { done() })
			mockConnect(mockSocket())
			expect(mockConnectHandler).toHaveBeenCalledTimes(1)
			expect(mockConnectHandler.mock.calls[0][0]).toBeInstanceOf(SkaarhojPanel)
		})
	})
	// it("should work!", () => {
	// 	const x = new SkaarhojServer((panel: SkaarhojPanel) => {
	// 		console.log(panel)
	// 	})
	// })
})

const mockSocket = (done?: () => void) => {
	const mockWrite = jest.fn().mockImplementationOnce((_, __, cb) => {
		cb()
		if (done) { done() }
	})
	return new Duplex({
		write: mockWrite,
		read: () => { }
	}) as Socket
}