import { SkaarhojPanel } from '../skaarhojPanel';
import net, { Socket } from 'net'
import { Duplex } from 'stream';
import { HWCAction } from '../models/skaarhoj';
jest.mock('net')

describe("SkaarhojPanel class", () => {
	it("creates an instance", () => {
		const x = new SkaarhojPanel(new net.Socket())
		expect(x).toBeInstanceOf(SkaarhojPanel)
	})

	describe('Lifecycle tests', () => {
		it("requests info on connect", async (done) => {
			const mockWrite = jest.fn().mockImplementationOnce((_, __, cb) => {
				cb()
				done()
			})
			const x = new SkaarhojPanel(new Duplex({
				write: mockWrite,
				read: () => { }
			}) as Socket)
			await new Promise(x => setTimeout(x, 3000))
			expect(mockWrite).toBeCalledTimes(1)
			// extract all data from the socket
			//const data = mockWrite.mock.calls.reduce((acc: string, call) => { return acc + call[0].toString() }, '')
			expect(getSocketData(mockWrite)).toContain('list\nmap\nPanelTopology?\nSleepTimer?\n')
		})

	})

	describe('socket input tests', () => {
		let panel: SkaarhojPanel,
			socket: Duplex,
			mockRead: jest.Mock,
			mockWrite: jest.Mock

		// reinstance the class before each test, to make sure the socket is clean
		beforeEach(async (done) => {
			mockRead = jest.fn()
			mockWrite = jest.fn().mockImplementation((_, __, cb) => { cb() })
			socket = new Duplex({
				write: mockWrite,
				read: mockRead,
			}) as Socket

			// mock implementation once to clear the mock socket of initial info req.
			mockWrite.mockImplementationOnce((_, __, cb) => { socket.push('_panelTopology_HWC={"HWc": [], "typeIndex": {}}\n'); cb() })
			panel = new SkaarhojPanel(socket as Socket)
			await new Promise(x => panel.on('ready', x))
			mockWrite.mockClear()
			done()
		})

		it('responds to "ping" with "pong"', (done) => {
			mockWrite.mockImplementation((data, __, cb) => {
				cb()
				expect(data.toString()).toMatch('pong\n')
				done()
			})

			socket.push('ping\n')
		})

		it('responds to "list" with "ActivePanel=1"', (done) => {
			mockWrite.mockImplementation((data, __, cb) => {
				cb()
				expect(data.toString()).toMatch('ActivePanel=1\n')
				done()
			})

			socket.push('list\n')
		})


		it('holds back data when "BSY" is received, and flushes when "RDY" is received', (done) => {
			mockWrite.mockImplementation((data, __, cb) => {
				cb()
				expect(data.toString()).toMatch('pong\n')
				done()
			})
			mockWrite.mockClear()
			socket.push('BSY\n')
			socket.push('ping\n')
			expect(mockWrite).not.toBeCalled()
			socket.push('RDY\n')
			// expect(mockWrite).toBeCalledTimes(1)
			// expect(getSocketData(mockWrite)).toMatch('pong\n')
		})

		it('sets model when "_model" is received', async () => {
			const prop = 'model'
			const waitForProp = new Promise(res =>
				panel.on('propertyChange', ({ key }) =>
					key === prop ? res(panel[prop]) : undefined))
			socket.push(`_${prop}=TESTPROP\n`)
			await expect(waitForProp).resolves.toMatch('TESTPROP')
		})

		it('sets serial when "_serial" is received', async () => {
			const prop = 'serial'
			const waitForProp = new Promise(res =>
				panel.on('propertyChange', ({ key }) =>
					key === prop ? res(panel[prop]) : undefined))
			socket.push(`_${prop}=TESTPROP\n`)
			await expect(waitForProp).resolves.toMatch('TESTPROP')
		})

		it('sets version when "_version" is received', async () => {
			const prop = 'version'
			const waitForProp = new Promise(res =>
				panel.on('propertyChange', ({ key }) =>
					key === prop ? res(panel[prop]) : undefined))
			socket.push(`_${prop}=TESTPROP\n`)
			await expect(waitForProp).resolves.toMatch('TESTPROP')
		})

		describe('HWC events', () => {
			it('correctly parses Down message', async (done) => {
				panel.on('hwc', (x) => {
					expect(x).toMatchObject({
						id: 1,
						mask: 1,
						action: HWCAction.Down,
						value: undefined
					})
					done()
				})
				socket.push('HWC#1.1=Down\n')
			})

			it('correctly parses Up message', async (done) => {
				panel.on('hwc', (x) => {
					expect(x).toMatchObject({
						id: 1,
						mask: 2,
						action: HWCAction.Up,
						value: undefined
					})
					done()
				})
				socket.push('HWC#1.2=Up\n')
			})

			it('correctly parses Press message', async (done) => {
				panel.on('hwc', (x) => {
					expect(x).toMatchObject({
						id: 1,
						mask: 8,
						action: HWCAction.Press,
						value: undefined
					})
					done()
				})
				socket.push('HWC#1.8=Press\n')
			})

			it('correctly parses Absolute message', async (done) => {
				panel.on('hwc', (x) => {
					expect(x).toMatchObject({
						id: 1,
						mask: undefined,
						action: HWCAction.Absolute,
						value: 777
					})
					done()
				})
				socket.push('HWC#1=Abs:777\n')
			})

			it('correctly parses Speed message', async (done) => {
				panel.on('hwc', (x) => {
					expect(x).toMatchObject({
						id: 2,
						mask: undefined,
						action: HWCAction.Speed,
						value: -100
					})
					done()
				})
				socket.push('HWC#2=Speed:-100\n')
			})

			it('correctly parses Relative message', async (done) => {
				panel.on('hwc', (x) => {
					expect(x).toMatchObject({
						id: 2,
						mask: undefined,
						action: HWCAction.Relative,
						value: -1
					})
					done()
				})
				socket.push('HWC#2=Enc:-1\n')
			})
		})
	})
})

const getSocketData = (mockWrite: jest.Mock): string => {
	return mockWrite.mock.calls.reduce((acc: string, call) => { return acc + call[0].toString() }, '')
}