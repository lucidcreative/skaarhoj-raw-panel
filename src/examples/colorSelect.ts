import { SkaarhojServer } from '../skaarhojServer'
import { SkaarhojPanel } from '../skaarhojPanel';
import { Brightness, TextOptions, ValueFormat, HardwareComponent } from '../models/skaarhoj';

const x = new SkaarhojServer(async (panel: SkaarhojPanel) => {
	// panel.on('log', x => console.log('LOG: ', x))
	// panel.on('rawData', x => console.log('RAW DATA: ', x))

	const colors = [
		'red',
		'orange',
		'yellow',
		'green',
		'blue',
		'purple',
	]

	panel.on('hwc', (x) => {
		if (x.action == 'Up') { panel.HWCs = baseHwcs }
		else {
			const btn = x.id
			const hwcs = JSON.parse(JSON.stringify(baseHwcs))
			const index = hwcs.indexOf(hwcs.find(x => x.id === btn)!)
			panel.setButtonColor([7, 8, 9, 10, 11, 12], hwcs[index].color, Brightness.Full)
			hwcs[index].color = 'white'
			hwcs[index].text.isLabel = !hwcs[index].text.isLabel
			panel.HWCs = hwcs
		}
	})

	await new Promise(res => panel.on('ready', res))
	const baseHwcs = Array.from(panel.HWCs.map((hwc, idx) => {
		const color = colors[idx % 6]
		return {
			...hwc,
			color: `hsl(${idx * 60},100%,50%)`,
			brightness: Brightness.Dimmed,
			text: {
				title: color,
				isLabel: false,
				valueOptions: {
					label1: "Hue",
					value1: idx * 60,
					format: ValueFormat.Integer
				}
			} as TextOptions
		}
	}))
	panel.HWCs = baseHwcs
	// panel.setButtonColor([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], "white")
	// panel.setDisplayText([1], { title: "Red", isLabel: false })
	// panel.setDisplayText([2], { title: "Orange", isLabel: false })
	// panel.setDisplayText([3], { title: "Yellow", isLabel: false })
	// panel.setDisplayText([4], { title: "Blue", isLabel: false })
	// panel.setDisplayText([5], { title: "Green", isLabel: false })
	// panel.setDisplayText([6], { title: "Purple", isLabel: false })

	panel.wake()
	// const startTs = Date.now()
	// const tick = 200
	// setInterval(() => {
	// 	panel.HWCs = panel.HWCs.map(rainbow((Date.now() - startTs) / tick / 10))
	// }, tick)
})

const rainbow = (offset: number) => (hwc, idx) => {
	const hue = ((offset + idx) * 60) % 360
	return {
		...hwc,
		color: `hsl(${hue},100%,50%)`,
		brightness: Brightness.Full,
		text: {
			title: 'Rainbow',
			isLabel: false,
			valueOptions: {
				value1: hue,
				format: ValueFormat.Integer
			}
		} as TextOptions
	}
}