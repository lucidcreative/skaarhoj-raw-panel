import { SkaarhojServer } from '../skaarhojServer'
import { SkaarhojPanel } from '../skaarhojPanel';
import { Brightness, TextOptions, ValueFormat, HardwareComponent } from '../models/skaarhoj';

/*
	This example was built for the Skaarhoj QuickBar, which has 6 buttons and 6 LED meters.
	If you're using a different panel, this might not look like anything.
*/

const x = new SkaarhojServer(async (panel: SkaarhojPanel) => {

	// panel.on('log', x => console.log('LOG: ', x))
	// panel.on('rawData', x => console.log('RAW DATA: ', x))

	// wait for the panel to be ready
	await new Promise(res => panel.on('ready', res))

	// wake the panel if it's in sleep mode
	panel.wake()

	// set up the update loop, 1200ms interval seems to be decent for it
	const startTs = Date.now()
	const tick = 120
	setInterval(() => {
		panel.HWCs = panel.HWCs.map(rainbow((Date.now() - startTs) / tick / 10))
	}, tick)
})

// this function takes the offset and rotates a hue value with it
// the initial hue is based off the index
const rainbow = (offset: number) => (hwc, idx) => {
	const hue = ((offset + idx) * 60) % 360
	return {
		...hwc,
		color: `hsl(${hue},100%,50%)`,
		brightness: Brightness.Full,
		text: {
			title: 'Rainbow',
			isLabel: false,
			valueOptions: {
				value1: hue,
				format: ValueFormat.Integer
			}
		} as TextOptions
	}
}