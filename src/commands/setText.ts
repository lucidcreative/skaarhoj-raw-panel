import { TextOptions } from '../models/skaarhoj';

/*
FROM SKAARHOJ DOCS

HWCt#xx=string Display text, tokenized string
xx is the HWc number, string is a string tokenized by a vertical pipe character,
“|”, where each position represents a given parameter being either an integer,
boolean or string.
The format of string follows this:

[value]|[format]|[fine]|[Title]|[isLabel]|[label 1 ]|[label 2]|[value2]|[values pair]|
[scale]|[scale range low]|[scale range high]|[scale limit low]|[scale limit high]|
[img]

string may not be longer than 63 chars
• [value] is a 16 bit integer representing the numerical value to be shown. If
empty, it will not render at all (like format=7).
• [format] defnes how [value] is formated: 0=Integer, 1=10e-3 Float w/2 dec.
points, 2=Percent, 3=dB, 4=Frames, 5=1/[value], 6=Kelvin, 7=Hidden,
8=10e-3 Float w/3 dec., 9=10e-2 Float w/2 dec., 10=1 Textline (Title &
value=size 1-4), 11=2 Textlines (Label 1, Label 2 & value=size 1-2). Default if
empty is Integer.
• [fine] is a boolean (0/1) that sets whether the fine indicator is shown under title bar.
• [Title] defnes the title string shown in the top of the display. Up to 10 chars
long.
• [isLabel] is a boolean (0/1) that sets if the title bar should be rendered as a
“label”. This is a convention used on SKAARHOJ controllers to indicate
whether the content of a display shows the state of a given parameter (the
current value) or if the display shows a label that indicates what will happen if
the associated control component is triggered. Default is to show “state”
which is indicated by a solid bar underlying the text. In “label” mode the title
is rendered with only a thin line underneath.
• [label 1] First text line under title. If [label 2] is omitted it will be printed in
large font (5 chars). Up to 10 chars long. If small text is preferred without invoking [label 2], please set [value2] to something.
• [label 2] Second text line under title. If not empty, both [label 1] and [label 2]
will print in small letters.
• [value2] Represents a second value. This is used if you use [label 1] and [label
2] as prefxes for [value] and [value2] along with settings for[values pair]
• [values pair] ranges from 1-4 and indicates 4 variations of boxing of value
pairs.
• [scale][scale range low]|[scale range high]|[scale limit low]|[scale limit high] indicates different types of scales in the bottom of the graphic that can show a
range of a given value.
• [img] is an index to a system stored media graphic fle.
Please check out the section later in this document for examples!

*/

export function setText(buttons: number[], textOptions: TextOptions): string {
	return buttons.map(btn => {
		const items = textOptions.valueOptions ?
			// version with values
			[
				// value
				textOptions.valueOptions.value1,
				// format
				textOptions.valueOptions.format || '',
				//fine
				textOptions.valueOptions.fine ? 1 : 0 || '',
				// title
				textOptions.title || '',
				// isLabel
				textOptions.isLabel ? 1 : 0,
				//label1 
				textOptions.valueOptions.label1 || '',
				// label2 
				textOptions.valueOptions.label2 || '',
				// value2 
				textOptions.valueOptions.value2,
				// values pair
				textOptions.valueOptions.valuePairing || '',
				// value scaling
				// TODO: implement scaling!
				'', '', '', '', '',
				// image
				// TODO: implement image!
				'',
			] :
			// version without values
			[
				// blanks for all values
				'', '', '',
				// title
				textOptions.title || '',
				// isLabel
				textOptions.isLabel ? 1 : 0,
				//blanks for labels and value 2 
				'', '', '', '',
				// value scaling
				// TODO: implement scaling!
				'', '', '', '', '',
				// image
				// TODO: implement image!
				'',
			]
		return `HWCt#${btn}=${items.join('|')}`
	}).join('\n')
}

