export { setColor } from './setColor'
export { setBrightness } from './setBrightness'
export { setText } from './setText'

export { wakeUp } from './wakeUp'

export { getInfo } from './getInfo'