import Color from 'color'

export function setColor(buttons: number[], color: string): string {
	return buttons.map(btn => `HWCc#${btn.toString(10)}=${rgbToInt(color).toString(10)}`).join('\n')
}

function rgbToInt(colorString: string): number {
	const c = Color(colorString)

	// convert to 4bit
	const fourBitColors = c.rgb().array().map(x => Math.floor(x / 64))
	// set bits 7 and 6 to one, then pack the colors
	return fourBitColors[2] + (fourBitColors[1] << 2) + (fourBitColors[0] << 4) + (1 << 6) + (1 << 7)
}

export const enum colors {
	white = 253,
	// warmWhite = parseInt('111101', 2),
	// red = parseInt('110000', 2),
	// rose = parseInt('110101', 2),
	// pink = parseInt('110011', 2),


}