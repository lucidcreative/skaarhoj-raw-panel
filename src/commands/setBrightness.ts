import { Brightness } from '../models/skaarhoj';

export function setBrightness(buttons: number[], brightness: Brightness): string {
	return buttons.map(btn => `HWC#${btn}=${brightness}`).join('\n')
}

