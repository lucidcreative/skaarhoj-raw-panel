import { Server, createServer } from 'net';
import { SkaarhojPanel } from './skaarhojPanel';

/*
	Skaarhoj Server class
	KBjordahl
	2019-08-13

	The server is responsible for listening for incoming connections from Skaarhoj
	control panels, and creates a Panel instance for each connection

*/

export class SkaarhojServer {

	static DEFAULT_PORT = 9923
	private server: Server

	constructor(
		private callback: (SkaarhojPanel) => void = () => { }
	) {
		this.server = createServer((socket) => callback(new SkaarhojPanel(socket)))
		this.server.listen(SkaarhojServer.DEFAULT_PORT)
	}

	public get active(): boolean {
		return this.server.listening;
	}

	stop() {
		this.server.close()
	}
}
