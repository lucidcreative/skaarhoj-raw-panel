import { Socket } from 'net';
import carrier from 'carrier';
import { Transform, Readable, Writable } from 'stream';
import { getInfo } from './commands/getInfo';
import { EventEmitter } from 'events';
import {
	HardwareComponent,
	Shape,
	Brightness,
	TextOptions,
	HWCEvent,
	LogEvent,
	DebugLogEvent,
	InfoLogEvent,
	WarnLogEvent,
	ErrorLogEvent,
	LogLevel,
	HWCAction,
	PropertyChangeEvent
} from './models/skaarhoj';
import { lookupType } from './models/lookupType';
import { setColor } from './commands/setColor';
import { setText } from './commands/setText';
import StrictEventEmitter from 'strict-event-emitter-types'
import { setBrightness, wakeUp } from './commands';
import { diff } from 'deep-object-diff'

/*
	Skaarhoj Panel class
	KBjordahl
	2019-08-13

	The panel class is responsible for communication with one Skaarhoj panel,
	and is created by the SkaarhojServer class
*/
type StrictEmitter = StrictEventEmitter<EventEmitter, PanelEvents>

interface PanelEvents {
	hwc: HWCEvent,
	ready: boolean
	log: LogEvent,
	debug: DebugLogEvent,
	info: InfoLogEvent,
	warn: WarnLogEvent,
	error: ErrorLogEvent,
	rawInput: string,
	rawOutput: string,
	propertyChange: PropertyChangeEvent,
}

export class SkaarhojPanel extends (EventEmitter as { new(): StrictEmitter }) {

	private outbound$: Readable
	private _model: string = ''
	private _serial: string = ''
	private _version: string = ''
	private _panelTopology_svgbase: string = ''
	private _isSleeping: string = ''
	private _sleepTimer: string = ''
	private _HWCs: HardwareComponent[] = []

	/**
	 * Constructor
	 * should only be called by the SkaarhojServer class, on connection of new panel
	 * @param _socket 
	 */
	constructor(private _socket: Socket) {
		// call super
		super()

		// set up outbound streaming
		this.outbound$ = new Readable({ read: () => { } })
		const delimitedOutput = this.outbound$.pipe(new LineDelimiter())
		delimitedOutput.pipe(this._socket)
		delimitedOutput.pipe(this.logPipe())

		// delimit the incoming by new lines and pass to the handler
		carrier.carry(this._socket, this._handleIncoming)

		// connect socket data to rawData event
		this._socket.on('data', (x) => {
			this.emit('rawInput', x.toString())
		})

		// this class is constructed when a panel connects, so we need to update some data from it
		this._requestPanelInfo()
	}

	/**
	 * the HWCs (Hardware Components) array is the internal reference for the structure of the panel
	 * 
	 * setting the HWCs will update any changes to the array items to be sent to the panel, including color, brightness and text.
	 */
	public get HWCs(): HardwareComponent[] {
		return JSON.parse(JSON.stringify(this._HWCs))
	}
	public set HWCs(v: HardwareComponent[]) {
		// send the diff between the value and the current ones
		this._sendHWCs(v, this._HWCs)

		// save the hwc
		this._HWCs = v
	}

	/**
	 * get the model name of the panel
	 */
	public get model(): string {
		return this._model
	}

	/**
	 * get the serial number of the panel
	 */
	public get serial(): string {
		return this._serial
	}

	/**
	 * get the version of the panel
	 */
	public get version(): string {
		return this._version
	}

	/**
	 * set the button color and brightness for one or more HWCs
	 */
	public setButtonColor = (ids: number[], color: string, brightness: Brightness = Brightness.Dimmed) => {
		// check for color on all hwc
		// if this filtered array has any values, they don't accept color
		const noColorHWC = ids.filter(id => !(this._HWCs.find(hwc => hwc.id === id)!.type!.hasColor))
		if (noColorHWC.length) {
			throw new Error(`The following HWCs do not have color: ${noColorHWC}`)
		}

		const msg = setColor(ids, color) + '\n' + setBrightness(ids, brightness)
		this.outbound$.push(msg)
		this._log(LogLevel.Debug, `sent color message: ${msg}`, 'SENT', { msg })

		// update our local hwcs
		this._HWCs.filter(x => ids.includes(x.id)).map(x => ({ ...x, color, brightness }))
	}

	/**
	 * set the display text for one or more HWCs
	 */
	public setDisplayText = (ids: number[], text: TextOptions) => {
		// check for text on all hwcs
		const noTextHWC = ids.filter(id => !(this._HWCs.find(hwc => hwc.id === id)!.type!.hasText))
		if (noTextHWC.length) {
			this._log(LogLevel.Warn, `The following HWCs do not have color: ${noTextHWC}`)
		}
		const msg = setText(ids, text)
		this.outbound$.push(msg)
		this._log(LogLevel.Debug, `sent color message: ${msg}`, 'SENT', { msg })

		// update our local hwcs
		this._HWCs.filter(x => ids.includes(x.id)).map(x => ({ ...x, text }))
	}

	/** 
	 *  wake the panel from sleep mode
	 */
	public wake = () => {
		this.outbound$.push(wakeUp())
	}


	// private methods 

	/** 
	 * handles the incoming lines from the carried socket (\n delimited)
	 */
	private _handleIncoming = (line: string) => {

		switch (true) {
			case line === 'ping':
				this.outbound$.push('pong')
				break

			case line === 'BSY':
				this._log(LogLevel.Warn, 'Panel is busy', 'PANEL_BUSY')
				this.outbound$.pause()
				break

			case line === 'RDY':
				this._log(LogLevel.Warn, 'Panel is ready', 'PANEL_READY')
				this.outbound$.resume()
				break

			case line === 'list':
				this.outbound$.push('\nActivePanel=1')
				this._sendHWCs(this._HWCs)
				break

			case line.startsWith('_'):
				this._setPrivateProperty(line)
				break

			case line.startsWith('HWC#'):
				this._handleHWCEvent(line)
				break

			case line.startsWith('map='):
				this._updateMap(line)
				break
		}
	}

	/**
	 * sends the initial panel info request
	 */
	private _requestPanelInfo = () => {
		this.outbound$.push(getInfo())
	}

	/**
	 * parses and emits the hwc event
	 */
	private _handleHWCEvent = (x: string) => {
		const [key, msg] = this.parseKeyValue(x)
		const [id, mask = undefined] = key.replace('HWC#', '').split('.').map(x => parseInt(x, 10))
		const [action, value = undefined] = msg.split(':')
		const event = {
			id: id as number,
			mask,
			action: action as HWCAction,
			value: value ? parseInt(value, 10) : undefined
		}
		this.emit('hwc', event)
		this._log(LogLevel.Info, `HWC Event: ${x}`, 'HWC_EVENT', { ...event })
	}

	/**
	 * sets a private member based on a response from the panel
	 * all the responses begin with '_'
	 */
	private _setPrivateProperty = (x: string) => {
		// this sets the properties received by the panel
		let [key, value] = this.parseKeyValue(x)

		switch (key) {
			case "_panelTopology_HWC":
				this.parsePanelTopology(value)
				key = '_HWCs'
				break
			default:
				this[key] = value
				this._log(LogLevel.Info, `Updated [${key}]=${value}`, 'PROPERTY_UPDATE', { key, value })
		}
		const publicKey = key.replace('_', '')
		this.emit('propertyChange', { key: publicKey })
	}

	/**
	 * updates the ids in the hwc list based on the panel mapping
	 * todo: figure out if changes in the map affect the library
	 */
	private _updateMap = (x: string) => {
		// this sets the properties received by the panel
		const [key, value] = this.parseKeyValue(x).map(x => parseInt(x, 10))
		if (this._HWCs[key - 1]) {
			this._HWCs[key - 1].id = value
			this._log(LogLevel.Info, `Map updated: ${x}`, 'UPDATE_MAP', { internalId: key, externalId: value })
		}
	}

	/**
	 * sends a diff of two hwc lists to the panel
	 */
	private _sendHWCs = (hwcs: HardwareComponent[], currentState: HardwareComponent[] = []) => {
		// determine the message to send with the changes
		// based on diffing each HWC object and generating only the strings
		// which have changed
		const msg = hwcs.map((hwc, index) => {
			const current = currentState.length > index ? currentState[index] : {}
			const d = diff(current, hwc) as HardwareComponent
			return [
				d.color ? setColor([hwc.id], d.color) : undefined,
				d.brightness ? setBrightness([hwc.id], d.brightness) : undefined,
				// for text, need to pull from non-diff, because of complex object
				d.text && hwc.text ? setText([hwc.id], hwc.text) : undefined,
			].join('\n')
		}).join('\n')

		// send the message
		this.outbound$.push(msg)
	}

	// Utility methods

	private parseKeyValue(x: string) {
		return x.split('=')
	}

	/**
	 * convert the provided panel topology json to an object
	 */
	private parsePanelTopology(x: string) {
		const topology = JSON.parse(x) as PanelTopology

		// handle types first
		const types = (Reflect.ownKeys(topology.typeIndex) as string[])
			.map((key) => lookupType(parseInt(key, 10), topology.typeIndex[key]))

		// now create HardwareControllers with types
		this._HWCs = topology.HWc.map<HardwareComponent>(({ x, y, txt, type }, index) => {
			return {
				...this._HWCs[index] || undefined,
				id: index + 1,
				position: { x, y },
				name: txt,
				type: types.find(x => x.id === type)!
			}
		})

		// as the map to be resent, just in case we missed it
		this.outbound$.push('map')

		this.emit('ready', true)
		this._log(LogLevel.Info, 'topology complete')
	}

	/**
	 * internal logging tool
	 */
	private _log = (level: LogLevel, message: string, code = '', data: {} = {}) => {
		const timestamp = Date.now()
		this.emit('log', { level, message, data, timestamp, code })
		switch (level) {
			case LogLevel.Debug:
				this.emit('debug', { level, message, data, timestamp, code })
				break
			case LogLevel.Info:
				this.emit('info', { level, message, data, timestamp, code })
				break
			case LogLevel.Warn:
				this.emit('warn', { level, message, data, timestamp, code })
				break
			case LogLevel.Error:
				this.emit('error', { level, message, data, timestamp, code })
				break
			default:
				break
		}
	}

	/**
	 * this is a helper function that makes the writable stream for generating logs + events from the outbound stream
	 */
	private logPipe = () => new Writable({
		write: (x, _, done) => {
			const str = x.toString()
			this._log(LogLevel.Debug, `SENT: ${str}`)
			this.emit('rawOutput', str)
			done()
		}
	})

}

/**
 * A utility class for appending line breaks to output data
 */
class LineDelimiter extends Transform {
	constructor() { super() }

	_transform(chunk: any, _, done) {
		this.push(chunk + '\n')
		done()
	}
}

interface PanelTopology {
	HWc: {
		x: number
		y: number
		txt: string
		type: number
	}[],

	typeIndex: {
		[key: string]: Shape
	}

}

